/*  Метод forEach() выполняет указанную функцию один раз для каждого элемента в массиве.
Метод forEach() используеться для перебора массива.Нельзя прервать или остановить цикл forEach().
Обычно этот цикл пишут,чтобы код был более-менее читаем.
*/

let items = ["hello", "world", 23, "23", null];

const filterBy = (arr, type) => arr.filter((item) => typeof item !== type);

console.log(filterBy(items, "string"));
